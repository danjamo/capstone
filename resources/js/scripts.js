/**
 * The alternative of this:
 * onclick="document.location = '/admin/{{ $user->id }}';"
 */
$(".clickable-row").on("click", function() {
    window.location = $(this).data("href");
});

/**
 * Fades away after a brief delay
 */
$('.pop-messages').fadeOut(4200, "linear");

/**
 * Enable tooltip
 * Calendar plugin
 */
$(function() {
    $('[data-toggle="tooltip"]').tooltip();

    $("#calendar").datepicker({
        // minDate: 0,
        maxDate: "+30d",
        beforeShowDay: function(date){ 
        var day = date.getDay(); 
        // return [day == 1 || day == 4,""];
        // highlight ONLY the particular days of the week
            if(date.getDay() == 2) {
                return [true, "lighted", 'Pay Dates'];
            } else {
                return [true, '', ''];
            }
        }
    });
});

    // $('#LoanModal').modal({
    //     keyboard: true,
    //     backdrop: "static",
    //     show: false,
    // }).on('show', function () {
    //     var getIdFromRow = $(this).data('id');
    //     $(this).find('#LoanRequestModalBody').html('<b> Order Id selected: ' + getIdFromRow + '</b>')
    // });

    // $(".table-striped").find('tr[data-target]').on('click', function () {
    //     //or do your operations here instead of on show of modal to populate values to modal.
    //     $('#LoanModal').data('id', getIdFromRow);
    // });

    // $('#LoanModal').modal({
    //     keyboard: true,
    //     backdrop: 'static',
    //     show: false,
    // }).on('show.bs.modal', function (event) {
    //     // var data_id = $(event.relatedTarget).data('id');

    //     // var data_id = $(event.relatedTarget).data('id');
    //     // var data_loan = $(event.relatedTarget).data('loan');
    //     // var data_days = $(event.relatedTarget).data('days');
    //     // var data_date = $(event.relatedTarget).data('date');

    //     var data = {
    //         data_id : $(event.relatedTarget).data('id'),
    //         data_loan : $(event.relatedTarget).data('loan'),
    //         data_days : $(event.relatedTarget).data('days'),
    //         data_date : $(event.relatedTarget).data('date'),
    //     };

    //     var json = JSON.stringify(data);

    //     // $(this).find("#LoanRequestModalBody").text(data_id);
    //     // $.post('request.blade.php', { 'data[]': [ data_id, data_loan, data_days, data_date ] });
    //     $.post('request.blade.php', { variable: json });
    // });

// $(document).ready(function() {
// 	$('#calendar').datepicker({
// 	   inline: true,
// 	   firstDay: 1,
// 	//    showOtherMonths: true,
// 	//    selectOtherMonths: true,
//        minDate: 0,
//        // Disable past dates(cant look prev months)
//        maxDate: "+30d",
//        // Deadline / Time allotted
// 	   beforeShowDay: function(date){ 
// 	     var day = date.getDay(); 
//          // return [day == 1 || day == 4,""];
//          // show a particular day
// 	     if(date.getDay() == 2) {
//              return [true, "Highlighted", ''];
//          } else {
//              return [true, '', ''];
//          }
// 	   }
// 	});
// });

