@extends('layouts.guest')

@section('title')
<title>Who we are</title>
@endsection

@section('content')
<div class="main-feature">
    <div class="container">
        <span class="h1 welcome-title">The Team</span>
        <hr>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-deck pt-3">
                    <div class="card border-0 align-items-center">
                        <img class="card-img-top rounded-circle dp" src="img/ag.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="text-center">Ansleigh Añora</h5>
                            <p class="card-text">Front End Developer / Back End Developer</p>
                        </div>
                    </div>
                    <div class="card border-0 align-items-center">
                        <img class="card-img-top rounded-circle dp" src="img/dj.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="text-center">Daniel Joseph Momo</h5>
                            <p class="card-text">Front End Developer / Back End Developer</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card-deck pt-3">
                    <div class="card border-0 align-items-center">
                        <img class="card-img-top rounded-circle dp" src="img/nc.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="text-center">Nike Marti Caballes</h5>
                            <p class="card-text">Front End Developer / Back End Developer</p>
                        </div>
                    </div>
                    <div class="card border-0 align-items-center">
                        <img class="card-img-top rounded-circle dp" src="img/jeff.png" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="text-center">Jefriel Cortes</h5>
                            <p class="card-text">Front End Developer / Back End Developer</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection