@extends('layouts.app')

@section('title')
<title>Terms and Condition</title>
@endsection

@section('content')
<h3 class="text-primary mt-3 display-4">Terms and Conditions</h3>
<div class="row my-4 mb-5">
    <div class="col">
        <div class="terms-content">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam ab ullam minima, voluptate molestiae aliquid asperiores voluptatum magnam nisi cupiditate fugiat molestias repellendus reiciendis possimus excepturi eaque. Fugit, quaerat labore!</p>
        </div>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        <div class="terms-title h5 text-info pb-3">Article Title 1</div>
        <div class="terms-content border-left border-info pl-5">
            <h6 class="terms-sub-title">Sub title 1</h6>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem nemo delectus earum quasi officiis praesentium quibusdam enim omnis, sequi rerum cumque ea id quia quo reiciendis velit! Veniam, fuga dicta.</p>
            <h6 class="terms-sub-title">Sub title 2</h6>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit et deserunt labore blanditiis commodi, ipsa recusandae temporibus quibusdam iusto minus ea deleniti odit, suscipit expedita ab reprehenderit illum quod mollitia?</p>
        </div>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        <div class="terms-title h5 text-success pb-3">Article Title 2</div>
        <div class="terms-content border-left border-success pl-5">
            <h6 class="terms-sub-title">Sub title 1</h6>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem nemo delectus earum quasi officiis praesentium quibusdam enim omnis, sequi rerum cumque ea id quia quo reiciendis velit! Veniam, fuga dicta.</p>
            <h6 class="terms-sub-title">Sub title 2</h6>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit et deserunt labore blanditiis commodi, ipsa recusandae temporibus quibusdam iusto minus ea deleniti odit, suscipit expedita ab reprehenderit illum quod mollitia?</p>
        </div>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        <div class="terms-title h5 text-warning pb-3">Article Title 3</div>
        <div class="terms-content border-left border-warning pl-5">
            <h6 class="terms-sub-title">Sub title 1</h6>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem nemo delectus earum quasi officiis praesentium quibusdam enim omnis, sequi rerum cumque ea id quia quo reiciendis velit! Veniam, fuga dicta.</p>
            <h6 class="terms-sub-title">Sub title 2</h6>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit et deserunt labore blanditiis commodi, ipsa recusandae temporibus quibusdam iusto minus ea deleniti odit, suscipit expedita ab reprehenderit illum quod mollitia?</p>
        </div>
    </div>
</div>
<div class="row my-4">
    <div class="col">
        <div class="terms-title h5 text-danger pb-3">Article Title 4</div>
        <div class="terms-content border-left border-danger pl-5">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem nemo delectus earum quasi officiis praesentium quibusdam enim omnis, sequi rerum cumque ea id quia quo reiciendis velit! Veniam, fuga dicta. Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi consequuntur iste magnam, voluptas ut unde nostrum atque architecto praesentium sit pariatur sapiente accusamus qui accusantium commodi laudantium rerum at obcaecati. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptates ut eaque mollitia numquam, molestias voluptatibus neque dolorem? Iusto, voluptates molestias. Odio temporibus debitis aperiam corrupti ducimus quam, ad iusto natus! Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sint distinctio esse voluptates, nobis nisi tempora laudantium vitae doloribus ratione quisquam possimus, consequatur dignissimos quaerat molestias sunt praesentium. Accusantium, sapiente libero.</p>
        </div>
    </div>
</div>
@endsection