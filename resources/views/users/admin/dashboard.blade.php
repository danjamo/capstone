@extends('layouts.app')

@section('title')
<title>Alkansya</title>
@endsection

@section('content')
<h3 class="mt-3">Dashboard</h3>
<div class="container">
    
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h3>Admin</h3>
                    You are logged in!
                    <h5>Transactions Table should be here</h5>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
