@extends('layouts.guest')

@section('title')
{{-- <title>{{ config('app.name', 'Alkansya') }}</title> --}}
<title>Welcome to Alkansya</title>
@endsection

@section('content')
<div class="main-feature">
    <div class="container">
        <div class="row align-items-center">
            <div class="col">
                <span class="welcome-title brand-title-header h2">Welcome to Alkansya</span>
                <div class="featured-desc py-2">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut earum doloremque quas a maiores amet expedita modi, ea magnam quis eum corrupti, deleniti quia tenetur eligendi illum tempora nemo eos?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis repudiandae hic nesciunt cum, non, recusandae quod dolor earum deleniti ipsam ipsum vitae fugiat aliquid quasi molestiae vel architecto mollitia, nobis.</p>
                </div>
                <div class="col col-sm col-md-6 col-lg-8 col-xl-6">
                    <div class="d-flex flex-column">
                        <a href="#" class="btn btn-outline-primary mw-50 p-2">I want to be a member</a>
                    </div>
                </div>
                <div class="pt-4">
                    <span class="h6">Already a member?</span> <a href="#">Login Here</a>
                </div>
            </div>
            <div class="col col-md-4 featured-img">
                <img src="img/img.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</div>

<div class="addon-feature mb-5">
    <div class="container-fluid">
        <div class="row">
            <div class="box bg-light col-md border border-white">
                <div class="w-25">
                    <img src="img/ez.png" alt="" class="img-fluid" style="max-height: 60px;">
                </div>
                <span class="h4">Easy</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur placeat aperiam iure, repellat ut tempora nobis rerum repellendus, esse ad iste fugiat dicta, sit est accusamus praesentium soluta quisquam voluptates.</p>
            </div>
            <div class="box bg-light col-md border border-white">
                <div class="w-25">
                    <img src="img/rr.png" alt="" class="img-fluid" style="max-height: 60px;">
                </div>
                <span class="h4">Fast</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, deleniti, vero? Consequatur nobis fuga eveniet modi, eos consequuntur optio tempore esse enim quod, alias ab adipisci possimus voluptatibus minus perferendis.</p>
            </div>
            <div class="box bg-light col-md border border-white">
                <div class="w-25">
                    <img src="img/tu.png" alt="" class="img-fluid" style="max-height: 60px;">
                </div>
                <span class="h4">Reliable</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat illo natus vitae laudantium labore, illum id quo deleniti similique tempora quis corporis nam facilis vero eligendi quibusdam architecto asperiores dolorem?</p>
            </div>
        </div>
    </div>
</div>
@endsection