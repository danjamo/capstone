<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanRequest extends Model
{
    // Table Name
    protected $table = 'loan_requests';

    // Primary key
    public $primaryKey = 'id';

    // Timestamps
    public $timestamps = true;

}
