<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CollectorsController extends Controller
{
    public function index() {
        return view('users.collector.dashboard')->with('active', 'dashboard');
    }
}
