<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use App\User;
use App\LoanRequest;
use Auth;
use Response;
use DB;

class LoanRequestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return LoanRequest::orderBy('created_at', 'desc')->where('user_id', Auth::user()->id)->get();
        // $requests = LoanRequest::orderBy('created_at', 'desc')->where('user_id', Auth::user()->id)->get();
        // return view('users.member.requests')->with('requests', $requests)->with('active', 'requests');

        if (Auth::user()->user_type == 2) {
            $requests = DB::table('loan_requests')->join('users', 'loan_requests.user_id', '=', 'users.id')->select('loan_requests.*', 'users.lname', 'users.fname', 'users.mname')->orderBy('loan_requests.created_at', 'desc')->whereNotNull('confirmed')->paginate(10);
            $pending = DB::table('loan_requests')->join('users', 'loan_requests.user_id', '=', 'users.id')->select('loan_requests.*', 'users.lname', 'users.fname', 'users.mname')->orderBy('loan_requests.created_at', 'desc')->whereNull('confirmed')->paginate(5);

            return view('users.admin.requests')->with('requests', $requests)->with('pending', $pending)->with('active', 'requests');
        } else {
            $requests = LoanRequest::orderBy('updated_at', 'desc')->where('user_id', Auth::user()->id)->whereNotNull('confirmed')->paginate(10);
            $pending = LoanRequest::orderBy('created_at', 'desc')->where('user_id', Auth::user()->id)->paginate(5);

            return view('users.member.requests')->with('requests', $requests)->with('pending', $pending)->with('active', 'requests');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.member.loan')->with('active', 'loan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return dd($request->input());

        $this->validate($request, [
            'amount' => ['required'],
            'days' => ['required'],
        ]);

        $loan = new LoanRequest;
        $loan->loan_amount = $request->input('amount');
        $loan->days_payable = $request->input('days');
        $loan->user_id = Auth::user()->id;
        $loan->save();

        // return dd($loan);
        
        return redirect()->action('LoanRequestsController@index');
        // return redirect()->action('LoanRequestsController@create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = LoanRequest::find($id);
        $request->delete();
        return redirect()->route('users-requests')->with('success', 'Request removed successfully');
    }

    public function accept($id) {
        $request = LoanRequest::find($id);
        $request->confirmed = true;
        $request->save();
        return redirect()->route('admin-requests')->with('success', 'Request Accepted');
    }

    public function reject($id) {
        $request = LoanRequest::find($id);
        $request->confirmed = false;
        $request->save();
        return redirect()->route('admin-requests')->with('error', 'Request Rejected');
    }
}
