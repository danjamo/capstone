<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/**
 * We dont have to use laravel's register view 
 * since that is the job of an Admin
 * @return redirects user to welcome page
 */
Route::get('/register', function() {
    return redirect('/');
});

/**
 * Sneaky users can't access these authenticated
 * URLs by typing it manually and also back button bug 
 * showing view is fixed with the middleware:
 * 'prevent-back-history'
 */
Route::middleware(['guest', 'prevent-back-history'])->group(function () {
    Route::view('/', 'welcome', ['active' => 'welcome']);
    Route::view('/about', 'about', ['active' => 'about']);
    Route::view('/terms', 'terms', ['active' => 'terms']);
});

/**
 * Route different roles of users with certain prefixes
 * 
 * "{type}-routes" Middleware restricts users from accessing
 * different URLs that does not belong to them, redirects
 * them back.
 * 
 */
use App\LoanRequest;

Route::middleware(['auth', 'prevent-back-history'])->group(function () {
    
    Route::prefix('admin')->middleware('admin-routes')->group(function () {
        View::share('counter', LoanRequest::where('confirmed', null)->count());
        Route::get('/dashboard', 'AdminsController@index')->name('admin-dashboard');
        Route::resource('/users', 'UsersController', [
            'names' => [
                'index' => 'users-index'
            ]
        ]);
        Route::get('/requests/{id}/accept', 'LoanRequestsController@accept');
        Route::get('/requests/{id}/reject', 'LoanRequestsController@reject');
        Route::resource('/requests', 'LoanRequestsController', [
            'names' => [
                'index' => 'admin-requests'
            ]
        ])->except([
            'edit', 'show', 'update'
        ]);
        Route::view('/terms', 'users.terms', ['active' => 'terms']);
    });

    Route::prefix('collector')->middleware('collector-routes')->group(function () {
        Route::get('/dashboard', 'CollectorsController@index')->name('collector-dashboard');
    });

    Route::prefix('member')->middleware('member-routes')->group(function () {
        Route::get('/dashboard', 'MembersController@dashboard')->name('member-dashboard');
        Route::get('/transactions', 'MembersController@transactions');
        // Route::get('/loan', 'MembersController@loan');
        Route::resource('/requests', 'LoanRequestsController', [
            'names' => [
                'index' => 'users-requests',
                'create' => 'member-loan-requests'
            ]
        ]);
        Route::view('/terms', 'users.terms', ['active' => 'terms']);
    });

});
